import sys
import os
import os.path
import numpy as np
import iris_processing
import time
from datetime import datetime
import pandas as pd

image_path = "..\\datasetIris"
images = iris_processing.Images(image_path)
# success_id = images.collect_images
success_images,success_id,success_label = images.collect_images

# print(success_id)
image_list = zip(success_id, success_label)
column_name = ['filename', 'label']
image_df = pd.DataFrame(image_list, columns=column_name)
image_df.to_csv('image_labels.csv', index=None)